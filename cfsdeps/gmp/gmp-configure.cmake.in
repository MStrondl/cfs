# -*- mode: cmake; coding: utf-8; indent-tabs-mode: nil; -*-
# kate: indent-width 2; encoding utf-8; auto-brackets on;
# kate: mixedindent on; indent-mode cstyle; line-numbers on;
# kate: syntax cmake; replace-tabs on; background-color #D1EBFF;
# kate: remove-trailing-space on; bracket-highlight-color #ff00ff;

#=============================================================================
# Set global variables, configured from the external build.
#=============================================================================
SET(gmp_source "@gmp_source@")
SET(gmp_install "@gmp_install@")
SET(CFS_BINARY_DIR "@CFS_BINARY_DIR@")
SET(CFS_ARCH "@CFS_ARCH@")
SET(LIB_SUFFIX "@LIB_SUFFIX@")
SET(CMAKE_TOOLCHAIN_FILE "@CMAKE_TOOLCHAIN_FILE@")
SET(APPLE "@APPLE@")

IF(CMAKE_TOOLCHAIN_FILE)
  INCLUDE("${CMAKE_TOOLCHAIN_FILE}")
ENDIF()

SET(ABI "64")

SET(ENV{CC} "@CMAKE_C_COMPILER@")
SET(ENV{CXX} "@CMAKE_CXX_COMPILER@")
# gmp must be configured without C_FLAGS and CXX_FLAGS !

find_program(SH sh)

SET(CONFIGURE_CMD "${SH}")
# see https://gmplib.org/manual/Build-Options.html
# and https://gmplib.org/manual/Notes-for-Package-Builds.html
# if you require maximum speed remove `--enable-fat` which will impact portability to other x86 cpu types
LIST(APPEND CONFIGURE_CMD 
  "./configure"
  "--prefix=${gmp_install}"
  "--bindir=${gmp_install}/bin"
  "--libdir=${gmp_install}/${LIB_SUFFIX}"
  "--includedir=${gmp_install}/include"
  "--enable-shared=no"
  "--enable-cxx"
  "--enable-fat"
  "ABI=${ABI}"
)

EXECUTE_PROCESS(
  COMMAND ${CONFIGURE_CMD}
  WORKING_DIRECTORY "${gmp_source}"
  ERROR_FILE gmp-config.err
  OUTPUT_FILE gmp-config.out
  RESULT_VARIABLE rv
  )
if(${rv})
  message("Configure of gmp failed")
  message("COMMAND: ${CONFIGURE_CMD}")
  message("WORKING_DIRECTORY: ${gmp_source}")
  message("ERROR_FILE: gmp-config.err")
  message("OUTPUT_FILE gmp-config.out")
endif(${rv})
